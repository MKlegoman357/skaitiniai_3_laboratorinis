import math

import matplotlib.pyplot as plt
import numpy as np

import algorithms
import utils


def f(x):
    """
    y=cos(2x)/(sin(2x)+1.5)-x/5; -2<=x<=3
    """
    return math.cos(2 * x) / (math.sin(2 * x) + 1.5) - x / 5


x_from = -2
x_to = 3

draw_f = True
draw_f_chebyshev = False
draw_f_chebyshev2 = False
draw_f_gynimas = True

if __name__ == "__main__":
    if draw_f:
        utils.draw_line_plot(f, x_from, x_to, steps=500, label="f(x)")

    if draw_f_chebyshev:
        x = algorithms.range_uniform_steps(x_from, x_to, steps=9)
        y = np.vectorize(f)(x)
        plt.scatter(x, y, label="interpoliavimo taskai")
        f_chebyshev = algorithms.construct_chebyshev_function(x, y)
        utils.draw_line_plot(f_chebyshev, x_from, x_to, steps=500, label="f(x) - Ciobysevo")

    if draw_f_chebyshev2:
        x = algorithms.range_by_chebyshev(x_from, x_to, steps=20)
        y = np.vectorize(f)(x)
        plt.scatter(x, y, label="Ciobysevo absices taskai")
        f_chebyshev2 = algorithms.construct_chebyshev_function(x, y)
        utils.draw_line_plot(f_chebyshev2, x_from, x_to, steps=500, label="f(x) - Ciobysevo (pagal Ciobysevo abscises)")

    if draw_f_gynimas:
        x = algorithms.range_uniform_steps(x_from, x_to, steps=4)
        y = np.vectorize(f)(x)
        plt.scatter(x, y, label="gynimas")
        f_gynimas = algorithms.construct_gynimas_function(x, y)
        utils.draw_line_plot(f_gynimas, x_from, x_to, steps=500, label="f(x) - Gynimas")

    plt.legend()
    plt.show()
