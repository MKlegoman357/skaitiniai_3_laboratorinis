import matplotlib.pyplot as plt
import numpy as np

import algorithms
import utils

if __name__ == "__main__":
    rows = utils.read_csv_file("spain_temperature_1991-2016.csv")
    rows = list(filter(lambda row: row[1] == 2012, rows))

    temperatures = [row[0] for row in rows]
    months = [row[2][:-len(" Average")] for row in rows]
    x_values = np.arange(len(temperatures))
    print(temperatures)
    plt.scatter(months, temperatures, label="duomenys")

    f_temperature = algorithms.construct_chebyshev_function(x_values, temperatures)
    utils.draw_line_plot(f_temperature, 0, len(temperatures) - 1, steps=500, label="Ciobysevo")

    hermite_spline_x, hermite_spline_y = algorithms.get_hermite_spline(x_values, temperatures, 101)
    plt.plot(hermite_spline_x, hermite_spline_y, label="Ermito (Akima)")

    plt.title("Ispanijos vidutine menesine temperatura 2012")
    plt.xticks(rotation=90)
    plt.legend()

    plt.show()
