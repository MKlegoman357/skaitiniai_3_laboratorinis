import matplotlib.pyplot as plt
import numpy as np

import algorithms
import utils

if __name__ == "__main__":
    rows = utils.read_csv_file("spain_temperature_1991-2016.csv")
    rows = list(filter(lambda row: row[1] == 2012, rows))

    temperatures = np.array([row[0] for row in rows])
    months = [row[2][:-len(" Average")] for row in rows]
    x_values = np.linspace(0, len(temperatures) - 1, len(temperatures))
    print(temperatures)
    plt.scatter(months, temperatures, label="duomenys")

    base_function_count = 5

    c = algorithms.get_simple_approximation_weights(x_values, temperatures, base_function_count)
    print(c)
    f_approximation = algorithms.construct_simple_approximation_function(c)

    utils.draw_line_plot(f_approximation, min(x_values), max(x_values), steps=500,
                         label=f"f(x) aproksimuota ({base_function_count})")

    plt.title("Ispanijos vidutine menesine temperatura 2012")
    plt.xticks(rotation=90)
    plt.legend()

    plt.show()
