import math

import matplotlib.pyplot as plt
import numpy as np

import algorithms
import utils

if __name__ == "__main__":
    country_code = "ES"
    country_part_index = 2
    number_of_points = 2158
    steps = 10
    show_points = True

    country_record, country_shape = utils.read_country_from_shapefile("countries/ne_10m_admin_0_countries", country_code)

    print("Parts:", country_shape.parts)

    country_part = country_shape.parts[country_part_index:country_part_index + 2]
    points = country_shape.points[country_part[0]:country_part[1]]

    print("Number of points:", len(points))

    space_between_points = math.floor(len(points) / number_of_points)
    points = [points[i * space_between_points] for i in range(number_of_points)]
    points.append(points[0])
    points = np.array(points)

    t_values = np.arange(len(points))
    x_values = points[:, 0]
    y_values = points[:, 1]

    if show_points:
        plt.scatter(x_values, y_values)

    x_spline_t_values, x_spline_x_values = algorithms.get_hermite_spline(t_values, x_values, steps)
    y_spline_t_values, y_spline_y_values = algorithms.get_hermite_spline(t_values, y_values, steps)

    plt.plot(x_spline_x_values, y_spline_y_values, color="red")

    plt.show()
