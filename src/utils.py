import csv

import matplotlib.pyplot as plt
import numpy as np
import shapefile

import algorithms


def draw_line_plot(f, x_from=None, x_to=None, step=None, steps=None, x=None, figure=plt, **kwargs):
    if x is None:
        x = algorithms.range_uniform_steps(x_from, x_to, step, steps)

    figure.plot(x, np.vectorize(f)(x), **kwargs)


def read_csv_file(path, ignore_lines=1):
    rows = []
    with open(path) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        lines = 0
        for row in csv_reader:
            lines += 1
            if lines > ignore_lines:
                rows.append(row)
                for i, item in enumerate(row):
                    try:
                        row[i] = int(item)
                    except ValueError:
                        try:
                            row[i] = float(item)
                        except ValueError:
                            row[i] = row[i].strip()
    return rows


def read_country_from_shapefile(path, isoa2_country_code):
    shp_reader = shapefile.Reader(path)

    with shp_reader as shp:
        country_record: shapefile._Record = None

        for record in shp.records():
            if record["ISO_A2"] == isoa2_country_code or record["NAME"] == isoa2_country_code:
                country_record = record
                break

        return country_record, shp.shape(country_record.oid)
